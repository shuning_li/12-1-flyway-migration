CREATE TABLE offices(
    id BIGINT,
    country VARCHAR(128),
    city VARCHAR(128),
    PRIMARY KEY (id)
);

CREATE TABLE staff(
    id BIGINT,
    first_name VARCHAR(128),
    last_name VARCHAR(128),
    office_id BIGINT,
    PRIMARY KEY (id),
    FOREIGN KEY (office_id) REFERENCES offices(id)
)