CREATE TABLE services(
    id BIGINT,
    description VARCHAR(128),
    PRIMARY KEY (id)
);

ALTER TABLE contracts ADD service_id  BIGINT, ADD FOREIGN KEY (service_id) REFERENCES services(id);
