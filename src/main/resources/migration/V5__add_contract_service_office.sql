CREATE TABLE contract_service_office(
    id          BIGINT,
    contract_id BIGINT,
    office_id   BIGINT,
    PRIMARY KEY (id),
    FOREIGN KEY (office_id) REFERENCES offices (id),
    FOREIGN KEY (contract_id) REFERENCES contracts (id)
)