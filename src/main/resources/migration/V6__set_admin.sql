ALTER TABLE contract_service_office
    ADD staff_id BIGINT,
    ADD FOREIGN KEY (staff_id) REFERENCES staff (id)